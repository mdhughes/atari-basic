# Atari Basic

All computers made after 1979 are trash.

Utilities for programming Atari Basic on a real or emulated Atari 800 or 800 XL, such as with Atari800MacX.

- Lines with --- comments are 1 before the entry point.
- Save into your H1 folder after replacing newline with char 155 (ATASCII newline).
- Be careful to work with it as 8-bit like Latin-1, not UTF-8! Inverse chars are ATASCII 128-255.

```
ENTER "H1:AUTO.LST"
RUN
```

## TODO:

- Cat: display contents of a text file
- Draw: pen size, lines, boxes, circles? START for menu, load, save
- Memo: single screen text editor, START for menu, load, save
